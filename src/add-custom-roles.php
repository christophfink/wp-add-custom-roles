<?php
/*
Plugin Name: add-custom-roles
Plugin URI: https://bitbucket.org/christophfink/wp-add-custom-roles
Description: add custom roles to wordpress
Version: 0.1
Author: Christoph Fink
Author URI: https://peippo.at
License: GPLv2
*/



// if called directly, exit immediately
defined("ABSPATH") or die("");



/**
 * plugin main class
 * 
 * This class is the main class of the ‘Add custom roles’ plugin.
 * It wraps all functionality of the plugin.
 *
 * @category IDontKnowWhatCategoryThisIs
 * @package  AddCustomRoles-WordpressPlugin
 * @author   Christoph Fink <christoph.fink@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-2.0.html gplv2
 * @link     https://4cities.eu/
 * @since    0.1
 */
class AddCustomRoles
{
    /** 
     * pluginDir
     *
     * remember home
     *
     * @since 0.1
     */
    private $_pluginDir;

    /**
     * defaultCapabilities
     *
     * the minimum rights a new role gets
     *
     * @since 0.1
     */
    private $_defaultCapabilites = array(
        "read"                       => false,
        "delete_posts"               => false,
        "edit_posts"                 => false,
        "delete_published_posts"     => false,
        "publish_posts"              => false,
        "upload_files"               => false,
        "edit_published_posts"       => false,
        "unfiltered_html"            => false,
        "read_private_pages"         => false,
        "edit_private_pages"         => false,
        "delete_private_pages"       => false,
        "read_private_posts"         => false,
        "edit_private_posts"         => false,
        "delete_private_posts"       => false,
        "delete_others_posts"        => false,
        "delete_published_pages"     => false,
        "delete_others_pages"        => false,
        "delete_pages"               => false,
        "publish_pages"              => false,
        "edit_published_pages"       => false,
        "edit_others_pages"          => false,
        "edit_pages"                 => false,
        "edit_others_posts"          => false,
        "manage_links"               => false,
        "manage_categories"          => false,
        "moderate_comments"          => false,
        "delete_site"                => false,
        "customize"                  => false,
        "edit_dashboard"             => false,
        "update_themes"              => false,
        "update_plugins"             => false,
        "update_core"                => false,
        "switch_themes"              => false,
        "remove_users"               => false,
        "promote_users"              => false,
        "manage_options"             => false,
        "list_users"                 => false,
        "install_themes"             => false,
        "install_plugins"            => false,
        "import"                     => false,
        "export"                     => false,
        "edit_users"                 => false,
        "edit_themes"                => false,
        "edit_theme_options"         => false,
        "edit_plugins"               => false,
        "edit_files"                 => false,
        "delete_users"               => false,
        "delete_themes"              => false,
        "delete_plugins"             => false,
        "create_users"               => false,
        "activate_plugins"           => false,
        "upload_themes"              => false,
        "upload_plugins"             => false,
        "manage_network_options"     => false,
        "manage_network_themes"      => false,
        "manage_network_plugins"     => false,
        "manage_network_users"       => false,
        "manage_sites"               => false,
        "manage_network"             => false,
        "delete_sites"               => false,
        "create_sites"               => false
    );

/**
     * addOptionsPage
     *
     * add an own options page in the WP backend
     * hooked to admin_menu action
     * option page is filled by ??? and ???
     *
     * @return void
     * @since 0.1
     */
    function addOptionsPage()
    {
        add_submenu_page(
            "options-general.php",
            "Add Custom Role(s)",
            "Add Custom Role(s)",
            "manage_options",
            "AddCustomRoles",
            function () {
                ?>
                <div class="wrap">
                    <h1>Add Custom Roles</h1>
                    <?php settings_errors(); ?>
                    <form method="post" action="options.php">
                    <?php
                        // This prints out all hidden setting fields
                        settings_fields("AddCustomRoles");
                        do_settings_sections("AddCustomRoles");
                        submit_button();
                    ?>
                    </form>
                </div>
                <?php
            }
        );
    }
    
    
    /**
     * registerSettings
     *
     * registers the settings this plugin wants to save
     * with Wordpress’ Settings API
     * called by the admin_init action hook
     *
     * @return void
     * @since 0.1
     */
    function registerSettings()
    {
        add_settings_section(
            "AddCustomRoles",
            "Add Custom Roles",
            function () {
                ?>
                    <p>
                       One role per line, capabilities in a space separated list after a colon: <code>rolename: capability1 capability2 …</code>
                    </p>
                <?php
            },
            "AddCustomRoles"
        );

        add_settings_field(
            "AddCustomRoles_CustomRoles",
            "Custom roles",
            function () {
                ?>
                    <textarea class="regular-text code" name="AddCustomRoles_CustomRoles"><?php echo get_option("AddCustomRoles_CustomRoles", "");?></textarea>
                <?php
            },
            "AddCustomRoles",
            "AddCustomRoles"
        );
        register_setting(
            "AddCustomRoles", 
            "AddCustomRoles_CustomRoles"
        );

    }
    
    /**
     * addCustomRoles
     *
     * actual worker function, 
     * adds/updates the custom roles
     * called by added_option_AddCustomRoles_CustomRoles_hook, 
     * update_option_AddCustomRoles_CustomRoles_hook
     * 
     * @since 0.1
     */
     function addCostumRoles()
     {
        $this->removeCustomRoles();

        $customRoles = get_option("AddCustomRoles_CustomRoles");
        try 
        {
            foreach (preg_split ('/$\R?^/m', $customRoles) as $line) 
            {
                try 
                {
                    $role = explode(":", $line)[0];
                    $capabilities = explode(
                        " ",
                        explode(":", $line)[1]
                    );
                    $roleCapabilities = $this->_defaultCapabilities();
                    foreach($capabilities as $c) 
                    {
                        if(array_key_exists($c, $roleCapabilities)) {
                           $roleCapabilities[$c] = true;
                        } 
                    }
                    add_role(
                        $role,
                        $role,
                        $roleCapabilities
                    );
                }
                catch(Exception $e) { continue; }
            }
        }
        catch(Exception $e) { continue; }

        // last: remember current options, so we know what changed next time
        update_option(
            "AddCustomRoles_CustomRoles_BAK",
            $customRoles
        );
     }
     
    /**
     * removeCustomRoles
     *
     * removes all (!) formerly added roles
     * call addCustomRoles afterwards to keep the configured ones
     *
     * @since 0.1
     */
     function removeCustomRoles()
     {
        $customRoles = get_option("AddCustomRoles_CustomRoles_BAK");
        try {
            foreach (preg_split ('/$\R?^/m', $customRoles) as $line) {
                try {
                    $role = explode(":", $line)[0];
                    remove_role($role);
                }
                catch(Exception $e) { continue; }
            }
        }
        catch(Exception $e) { continue; }
     }
     
    /**
     * deactivatePlugin
     *
     * remove ‘our’ roles and options
     *
     * @since 0.1
     */
     function deactivatePlugin()
     {
        $this->removeCustomRoles();
        delete_option("AddCustomRoles_CustomRoles");
        delete_option("AddCustomRoles_CustomRoles_BAK");
     }
     
    /**
     * constructor for AddCustomRoles
     * 
     * constructor function of the main class
     * of the plugin
     * establishes hooks to wp_actions,
     * registers scripts and stylesheets
     *
     * @since 0.1
     */
    function __construct() 
    {
        /* remember home */
        $this->_pluginDir = plugin_dir_url(__FILE__);
        
        /* create options page in the backend */
        add_action(
            "admin_menu",
            array(&$this, "addOptionsPage")
        );
            
        /* register our plugin’s settings */
        add_action(
            "admin_init",
            array(&$this, "registerSettings")
        );
        
        /* update roles when option is updated */
        add_action(
            "added_option_AddCustomRoles_CustomRoles_hook",
            array(&$this, "addCustomRoles")
        );
        add_action(
            "update_option_AddCustomRoles_CustomRoles_hook",
            array(&$this, "addCustomRoles")
        );
        
        /* remove our roles and options when plugin is deactivated */
        register_deactivation_hook(
            __FILE__,
            array(&$this, "deactivatePlugin")
        );
    }
}

/* create the instance */
global $addCustomRoles;
$addCustomToles = new AddCustomRoles;

?>

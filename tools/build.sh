#!/bin/bash

origDir="$(pwd)"
cd "$(dirname "${0}")"/..

rsync -azH --partial --delete --exclude '*.swp' --exclude '**/*.swp' --exclude '~*' --exclude '**/~*' src/ build/
cd build


find . -iname "*.js"|while read scriptFile;do 
	if [[ "$2" == "-t" ]]; then 
        	continue
	fi
	if [[ "${scriptFile/%.min.js/}" != "${scriptFile}" ]]; then # if file is already *.min.js
		continue;
	fi;

	uglifyjs -c -m --stats "$scriptFile" > "${scriptFile/%.js/.min.js}"
	rm "$scriptFile"
	scriptFile="${scriptFile/#.\/}"
	scriptFile="${scriptFile//\//\\/}"
	find . -iname "*.html" -or -iname "*.php"|while read markupFile;do
		#echo "${markupFile}"
		sed "s/${scriptFile/./\\.}/${scriptFile/%.js/.min.js}/g" < "${markupFile}" > "${markupFile}.new"
		mv "${markupFile}.new" "${markupFile}"
	done
done

find . -iname "*.css"|while read styleFile;do
	if [[ "$2" == "-t" ]]; then
		continue
	fi
	if [[ "${styleFile/%.min.js/}" != "${styleFile}" ]]; then
		continue;
	fi;
	if [[ "${styleFile}" == "./style.css" ]];then
		continue;
	fi;
	echo "$styleFile"
	csso "$styleFile" > "${styleFile/%.css/.min.css}"
	rm "$styleFile"
	styleFile="${styleFile/#.\/}"
	styleFile="${styleFile//\//\\/}"
	find . -iname "*.html" -or -iname "*.php"|while read markupFile;do
		#echo "${markupFile}"
		sed "s/${styleFile//./\\.}/${styleFile/%.css/.min.css}/g" < "${markupFile}" > "${markupFile}.new"
		mv "${markupFile}.new" "${markupFile}"
	done
done

cd ..

if [[ "$1" == "-u" ]];then # upload to test server
    rsync -azvH --rsh=ssh --progress --partial --delete --exclude ".keep" build/ virthost.vub.ac.be:/u/forcitie/development/wp-content/plugins/add-custom-roles/
fi;

cd "$origDir"
